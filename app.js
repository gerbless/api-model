'use strict'
//MODULOS REQUERIDOS PARA EJECUTAR LA APLICACION EXPRESS
const express = require('express'),
    favicon = require('serve-favicon'),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    restFull = require('express-method-override')('_method'),
    routes = require('./routes/index'),
    faviconUrl = `${__dirname}/public/img/node-favicon.png`,
    publicDir = express.static(`$(__dirname)/public`),
    ViewDir = `${__dirname}/views`,
    port = (process.env.PORT || 8080),
    app = express()
   /* GoogleSpreadsheet = require('google-spreadsheet'),
    SPREADSHEET_ID = '1H7xmQV435NNwN9P_890FAp36-gr_p8F8O4kBO_7tPhk',
    async = require('async')

    let doc = new GoogleSpreadsheet(SPREADSHEET_ID);
    let sheet; 

    async.series([
        function setAuth(step) {
          // see notes below for authentication instructions!
          var creds = require('./service-account.json');
          doc.useServiceAccountAuth(creds, step);
        },
        function getInfoAndWorksheets(step) {
          doc.getInfo(function(err, info) {
            //console.log('Loaded doc: '+info.title+' by '+info.author.email);
            sheet = info.worksheets[0];
            //console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
            step();
          });
        },
        function workingWithCells(step) {
            sheet.addRow({'Nombres':'Germain'},function(err, cells){
                console.log(err)
            });
        },

      ], function(err){
          if( err ) {
            console.log('Error: '+err);
          }
      });*/
//configurando app
app
    .set('views',ViewDir)
    .set('view engine','jade')
    .set('port',port)
    .use(function (req, res, next) {
          //res.setHeader('Access-Control-Allow-Origin','http://localhost:8101');
          res.setHeader('Access-Control-Allow-Origin','*')
          //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
          res.setHeader('Access-Control-Allow-Methods', '*')
          //res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,application/x-www-form-urlencoded')
          res.setHeader('Access-Control-Allow-Headers', '*')
          next()
        }
    )
    //ejecutando middlewaress
    .use(favicon(faviconUrl))
    // parse application/x-www-form-urlencoded
    .use(bodyParser.urlencoded({extended:true}))
    // parse application/json
    .use(bodyParser.json())
    .use(restFull)
    .use(morgan('dev'))
    .use(publicDir)
    .use(routes)

module.exports = app
