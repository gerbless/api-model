'use strict'

const AppControllers	= require('../config/my-controllers'),
	express = require('express'),
	//Auth   = require("../middlewares/auth"),
	routes = express.Router()

routes
		.get('/api/users',AppControllers.Users.getAll)
		.get('/api/users/:id',AppControllers.Users.getFind)
		.post('/api/users/add',AppControllers.Users.save)
		.put('/api/users/update/:id',AppControllers.Users.save)
		.delete('/api/users/delete/:id',AppControllers.Users.destroy)
		//.delete('api/apps/delete/:id',Auth.isAuth,AppControllers.Aplicaciones.destroy)

		.use(AppControllers.Users.Error404)

module.exports = routes