const GoogleSpreadsheet = require('google-spreadsheet'),
    SPREADSHEET_ID = '1H7xmQV435NNwN9P_890FAp36-gr_p8F8O4kBO_7tPhk',
    async = require('async'),
    GoogleSheetsController = () => {}

let doc = new GoogleSpreadsheet(SPREADSHEET_ID),
    sheet;

GoogleSheetsController.newRow = (data,hoja) =>
{
    async.series([
        function setAuth(step) {
            // see notes below for authentication instructions!
            var creds = require('./service-account.json');
            doc.useServiceAccountAuth(creds, step);
          },
          function getInfoAndWorksheets(step) {
            doc.getInfo(function(err, info) {
              console.log('Loaded doc: '+info.title+' by '+info.author.email);
              sheet = info.worksheets[hoja];
              console.log('sheet 1: '+sheet.title+' '+sheet.rowCount+'x'+sheet.colCount);
              step();
            });
          },
          function workingWithCells() {
            sheet.addRow(data,function(err, cells){
                console.log(err)
            });
        },
    ])

}

module.exports = GoogleSheetsController
