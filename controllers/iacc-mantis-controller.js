'use strict'
const http = require('http'),
	sa = require('superagent'),
	sheets = require('./sheets'),
    moment = require('moment'),
	MantisController = () => {}

MantisController.save = (req,res,next) => 
{
	const datosJson = JSON.parse(req.body["data.json"]),
		  Data = {
			page_url : req.body.page_url,
			page_name: req.body.page_name,
			page_id : req.body.page_id,
			variant: req.body.variant,
			rut: datosJson.rut.toString(),
			time_submitted : datosJson.time_submitted.toString(),
			device : datosJson.device.toString(),
			page_uuid : datosJson.page_uuid.toString(),
			origen : datosJson.MID.toString(),
			nombre : datosJson.nombre.toString(),
			apellido_paterno : datosJson.apellido.toString(),
			apellido_materno : null,
			utm_term: null,
			email : datosJson.email.toString(),
			date_submitted : datosJson.date_submitted.toString(),
			carrera : datosJson.carrera.toString(),
			utm_content : datosJson.utm_content.toString(),
			utm_source : datosJson.utm_source.toString(),
			utm_medium : datosJson.utm_medium.toString(),
			telefono : datosJson.numeromovil.toString(),
			utm_campaign : datosJson.utm_campaign.toString(),
			ip_address : datosJson.ip_address.toString(),
			'user-agent' : datosJson.UserAgent.toString()
		}
	//https://www.inw.cl/iacc/ws/formulario.php
	//http://uatmantis.iacc.cl/ws/formulario.php
	MantisController.send(Data,'crm-admision.iacc.cl/ws/formulario.php',res)
	
}


MantisController.send = (data,url,resp) => {
	sa.post(url)
	 .send(data)
	 .end(function(err, res) {
        const result = JSON.parse(res.text)
         if(result.des_respuesta == "OK"){
            resp.status(200).json(result.des_respuesta)
         }else{
            resp.status(400).json(result.des_respuesta)
		 }
		 sheets.newRow({
			respuestaApi:result.des_respuesta,
			rut : data.rut,
			nombre : data.nombre,
			apellido : data.apellido_paterno,
			email : data.email,
			telefono : data.telefono,
			carrera : data.carrera,
			tiempo:moment().format('LLL'),
			page_name: data.page_name,
		},0)
		 console.log(result.des_respuesta)
	});
}

MantisController.Error404 = (req,res,next) => {

	let error = new Error(),
		locals = {
			title: 'Error 404',
			description: 'Recurso No Encontrado',
			error: error
		}
	error.status = 404
	res.render('error',locals)
	next()
}


module.exports = MantisController




