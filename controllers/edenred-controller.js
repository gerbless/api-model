'use strict'
const request = require('request'),
	sa = require('superagent'),
	sheets = require('./sheets'),
    moment = require('moment'),
    firebase = require("firebase-admin"),
    serviceAccount = require("./serviceAccountKey.json"),
    EdenredController = () => {}

    EdenredController.add = (req,res,next) =>
{
    //const datosJson = JSON.parse(req.body["data.json"]),
    //    url ='http://requestbin.fullcontact.com/xcm189xc'

    /*request.post(url,{form:{
        'respuestaApi' : null,
        'oid' : datosJson.oid,
        'Motivo_de_Contacto__c' : datosJson.Motivo_de_Contacto__c,
        'lead_source' : datosJson.lead_source,
        'first_name' : datosJson.name,
        'last_name' : datosJson.apellido,
        'RUT__c' :datosJson.rut,
        'mobile' : datosJson.numeromovil,
        'email' : datosJson.email,
        'idFormulario__c' : moment().format("YYYYMMDDHHmmssSSSS"),
        'company' : datosJson.comercio,
        'device' : datosJson.device,
        'page_name' : datosJson.page_name,
        'mensaje' : datosJson.mensaje,
        'utm_content' : datosJson.utm_content,
        'utm_source' : datosJson.utm_source,
        'utm_medium' : datosJson.utm_medium,
        'utm_campaign' : datosJson.utm_campaign,
        'ip_address' : datosJson.ip_address,
        'tiempo' : moment().format('LLL')
    }}
    ,
    function (error, response, body) {
        if (!error) {
            console.log(body)
        }
    });*/
    const datosJson = JSON.parse(req.body["data.json"]),
          Data = {
            respuestaApi : null,
            oid : String(datosJson.oid),
            Motivo_de_Contacto__c : String(datosJson.Motivo_de_Contacto__c),
            lead_source : String(datosJson.lead_source),
            first_name : String(datosJson.nombre),
            last_name : String(datosJson.apellido),
            RUT__c :String(datosJson.rut),
            mobile : String(datosJson.numeromovil),
            email : String(datosJson.email),
            idFormulario__c : moment().format("YYYYMMDDHHmmssSSSS"),
            company : String(datosJson.comercio),
            device : String(datosJson.device),
            page_name : String(datosJson.page_name),
            mensaje : String(datosJson.mensaje),
            utm_content : String(datosJson.utm_content),
            utm_source : String(datosJson.utm_source),
            utm_medium : String(datosJson.utm_medium),
            utm_campaign : String(datosJson.utm_campaign),
            ip_address : String(datosJson.ip_address),
            tiempo : moment().format('LLL'),
          }

    EdenredController.send(Data,'http://requestbin.fullcontact.com/xgvowmxg',res)
     //EdenredController.send(Data,res)
}


EdenredController.send = (data,url,resp) => {
	sa.post(url)
     .set('Content-Type', 'application/x-www-form-urlencoded')
	 .send(data)
	 .end(function(err, res) {
        const Code = res.statusCode
         if(Code== 200){
            resp.status(Code).json(Code)
         }else{
            resp.status(Code).json(Code)
         }
         data.respuestaApi = Code
         sheets.newRow(data,2)

         firebase.initializeApp({
            credential: firebase.credential.cert(serviceAccount),
            databaseURL: "https://edenred-bd.firebaseio.com"
        });

        var db = firebase.database();
        var ref = db.ref("edenred-bd");
        ref.on("child_added", function(snapshot) {
        // Aquí podría desarrollar una funcionalidad de mantenimiento de mi app,
        // que se ejecutará cada vez que se crea un nuevo elemento en "users".
        // De momento coloco simplemente unos mensajes en la consola.
        var el = snapshot.val();
        console.log(el);
        console.log(snapshot.key);
        });

	});
}

module.exports = EdenredController

