'use strict'
const http = require('http'),
    sa = require('superagent'),
    sheets = require('./sheets'),
    moment = require('moment'),
	MantisController = () => {}

MantisController.saveWorpress = (req,res,next) => 
{
	//const datosJson = JSON.parse(req.body["data.json"]),
	const  Data = {
            rut : req.body.rut,
            nombre : req.body.nombre,
            apellido_paterno : req.body.apellido_paterno,
            apellido_materno : req.body.apellido_materno,
            email : req.body.email,
            telefono : req.body.telefono,
            carrera : req.body.carrera,
            origen : '186',
            utm_source : req.body.utm_source,
            utm_medium : req.body.utm_medium,
            utm_campaign : req.body.utm_campaign,
            utm_term : req.body.utm_term,
            utm_content : req.body.utm_content,
            device : req.body.device,
            'user-agent' : req.body.User_Agent,
            'ip_address' : req.body.ip_address,
        }
    //http://10.77.82.73/ws/formulario.php
    //http://uatmantis.iacc.cl/ws/formulario.php
    //http://mantis.iacc.cl/ws/formulario.php
	MantisController.send(Data,'crm-admision.iacc.cl/ws/formulario.php',res)
}

//TSCG
MantisController.send = (data,url,resp) => {
	sa.post(url)
	 .send(data)
	 .end(function(err, res) {
        const result = JSON.parse(res.text)
         if(result.des_respuesta == "OK"){
            resp.status(200).json(result.des_respuesta)
         }else{
            resp.status(400).json(result.des_respuesta)
         }
         sheets.newRow({
             respuestaApi:result.des_respuesta,
             rut : data.rut,
             nombre : data.nombre,
             apellido : data.apellido_paterno,
             email : data.email,
             telefono : data.telefono,
             carrera : data.carrera,
             tiempo:moment().format('LLL')
         },1)
         console.log(result.des_respuesta)
	});
}

MantisController.Error404 = (req,res,next) => {

	let error = new Error(),
		locals = {
			title: 'Error 404',
			description: 'Recurso No Encontrado',
			error: error
		}
	error.status = 404
	res.render('error',locals)
	next()
}


module.exports = MantisController

//Client ID
//608700122154-41kv4pm6na9st2af8kkjc00lhjvl833t.apps.googleusercontent.com

//Client Secret
//q5usVoSiKncIMNhzdXzWxHQE