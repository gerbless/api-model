'use strict'

const AppControllers	= require('../config/my-controllers'),
	express = require('express'),
	//Auth   = require("../middlewares/auth"),
	routes = express.Router()

routes
		.post('/api/mantis/add',AppControllers.IaccMantis.save)
		.post('/api/mantis/worpress/add',AppControllers.IaccMantisWorpress.saveWorpress)
		.post('/api/edenred/add',AppControllers.Edenred.add)

		.use(AppControllers.IaccMantis.Error404)

module.exports = routes