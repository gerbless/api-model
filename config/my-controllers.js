'use strict'
const IaccMantis	= require('../controllers/iacc-mantis-controller'),
	IaccMantisWorpress	= require('../controllers/iacc-mantis-worpress'),
	Edenred	= require('../controllers/edenred-controller')
module.exports = {
	IaccMantis,
	IaccMantisWorpress,
	Edenred
}